import zmq
import time
import sys

port = "5555"
context = zmq.Context()
print("Connecting to server...")
socket = context.socket(zmq.PAIR)
socket.connect ("tcp://localhost:%s" % port)
if len(sys.argv) > 2:
    socket.connect ("tcp://localhost:%s" % port1)

for request in range (1,10):
    print("Sending request ", request,"...")
    socket.send_string("Hello")
    #  Get the reply.
    message = socket.recv()
    print("Received reply ", request, "[", message, "]")